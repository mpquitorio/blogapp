@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @include('inc.menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('home') }}" class="btn btn-default mb-2 p-0 text-muted"><i class="far fa-arrow-alt-circle-left"></i> Go Back</a>
                        <h2 class="card-title">{{ $post->title }}</h2>
                        <p class="text-muted">by {{ $post->user->name }} <br><small class="text-muted">Last updated on {{ $post->updated_at->format('d M Y @ h:i A') }}</small></p>
                        <hr>
                        <img src="{{ $post->featured }}" alt="" width="100%">
                        <hr>
                        <p class="class-text">{{ $post->content }}</p>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection