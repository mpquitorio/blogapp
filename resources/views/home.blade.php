@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @include('inc.menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header font-weight-bold">{{ config('app.name') }} : Dashboard</div>
                    <div class="card-body">
                        <h4>Your Blog Posts</h4>
                        @if( count($posts) > 0 )
                            <table class="table table-striped">
                                <thead class="text-muted">
                                    <tr>
                                        <th width="40%">Post Title</th>
                                        <th width="20%">Date Created</th>
                                        <th width="30%">Last Updated</th>
                                        <th width="10%" colspan="3" class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($posts as $post)
                                        <tr>
                                            <td width="40%"><a href="{{ route('post.show', ['id' => $post->id]) }}" class="text-decoration-none">{{ $post->title }}</a></td>
                                            <td width="20%">{{ $post->created_at->format('M d, Y') }}</td>
                                            <td width="30%">{{ $post->updated_at->format('d M Y @ h:i A') }}</td>
                                            <td width="5%" class="p-1"><a href="{{ route('post.edit', ['id' => $post->id]) }}" class="btn btn-success"><i class="fas fa-edit"></i></a></td>
                                            <td width="5%" class="p-1">
                                                <form action="{{ route('post.destroy', ['id' => $post->id]) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        {{ $posts->links() }}
                        @else
                            <hr>
                            <p>You have no posts.</p>
                        @endif
                    </div>
                    <div class="card-body">
                        <h4>Post Category</h4>
                        @if( count($categories) > 0 )
                            <table class="table table-striped">
                                <thead class="text-muted">
                                    <th width="40%">Name</th>
                                    <th width="20%">Date Created</th>
                                    <th width="30%">Last Updated</th>
                                    <th width="10%" colspan="2" class="text-center">Actions</th>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td width="40%">{{ $category->name }}</td>
                                            <td width="20%">{{ $category->created_at->format('d M Y') }}</td>
                                            <td width="30%">{{ $category->updated_at->format('d M Y @ h:i A') }}</td>
                                            <td width="5%" class="p-1">
                                                <a href="{{ route('category.edit', ['id' => $category->id]) }}" class="btn btn-success"><i class="fas fa-edit"></i></a>
                                            </td>
                                            <td width="5%" class="p-1">
                                                <form action="{{ route('category.destroy', ['id' => $category->id]) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <hr>
                            <p>No category available.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
