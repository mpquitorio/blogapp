<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use Session;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.posts.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'    => 'required',
            'featured' => 'required|image|max:1999',
            'content'  => 'required',
            'category' => 'required'
        ]);

        // get the filename extension
        $featuredExt = $request->file('featured')->getClientOriginalExtension();
        // make a new image filename
        $featuredNewName = uniqid().'.'.$featuredExt;
        // move to the uploads folder
        $path = $request->file('featured')->move('uploads/posts', $featuredNewName);

        $post = new Post;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->category_id = $request->input('category');
        $post->featured = '/uploads/posts/'.$featuredNewName;
        $post->user_id = auth()->user()->id;
        $post->save();

        Session::flash('success', 'You successfully created a post!');
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $categories = Category::all();

        return view('admin.posts.edit')->with('post', $post)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'    => 'required',
            'featured' => 'image|max:1999',
            'content'  => 'required',
            'category' => 'required'
        ]);

        if($request->hasFile('featured'))
        {
            // get the filename extension
            $featuredExt = $request->file('featured')->getClientOriginalExtension();
            // make a new image filename
            $featuredNewName = uniqid().'.'.$featuredExt;
            // move to the uploads folder
            $path = $request->file('featured')->move('uploads/posts', $featuredNewName);
        }

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->category_id = $request->input('category');
        if($request->hasFile('featured'))
        {
            $post->featured = '/uploads/posts/'.$featuredNewName;
        }
        $post->save();

        Session::flash('success', 'You successfully updated a post!');
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        Session::flash('success', 'You successfully deleted a post!');
        return redirect()->route('home');
    }
}
