@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Blogs</h1>
        <hr>
        @if( count($posts) > 0 )
            <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                        <h5 class="card-header">Categories</h5>
                        <div class="card-body">
                            <ul class="list-group">
                                @foreach ($categories as $category)
                                    <li class="list-group-item">
                                        <a href="{{ route('selectedCategory', ['id' => $category->id]) }}" class="text-decoration-none">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="card-deck mb-3">
                        @foreach ($posts as $post)
                            <div class="col col-md-6 p-0">
                                <div class="card">
                                    <img class="card-img-top" src="{{ $post->featured }}" alt="" width="100%">
                                    <div class="card-body">
                                        <h2 class="card-title">{{ $post->title }}</h2>
                                        <p class="card-text">{{ substr($post->content, 0, 210) }} ...</p>
                                        <a href="{{ route('show', ['id' => $post->id]) }}" class="btn btn-primary">Read More &rarr;</a>
                                    </div>
                                    <div class="card-footer text-muted">
                                        <small class="text-muted">Last updated on {{ $post->updated_at->format('M d, Y') }} by {{ $post->user->name }}</small>
                                    </div>
                                </div>
                            </div>   
                        @endforeach
                    </div> 
                    {{ $posts->links() }}                          
                </div>
            </div>
        @else
            <p>No posts available.</p>
        @endif
    </div>

@endsection