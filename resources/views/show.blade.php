@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Blogs</h1>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <h5 class="card-header">Categories</h5>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach ($categories as $category)
                                <li class="list-group-item">
                                    <a href="{{ $category->id }}">{{ $category->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('index') }}" class="btn btn-default mb-2 p-0 text-muted"><i class="far fa-arrow-alt-circle-left"></i> Go Back</a>
                        <h2 class="card-title">{{ $post->title }}</h2>
                        <p class="text-muted">by {{ $post->user->name }} <br><small class="text-muted">Last updated on {{ $post->updated_at->format('d M Y @ h:i A') }}</small></p>
                        <hr>
                        <img src="{{ $post->featured }}" alt="" width="100%">
                        <hr>
                        <p class="class-text">{{ $post->content }}</p>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection