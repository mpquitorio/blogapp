<div class="col-lg-3">
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route('post.create') }}" class="text-decoration-none"><i class="fas fa-plus-circle"></i> Create Post</a></li>
        <li class="list-group-item"><a href="{{ route('category.create') }}" class="text-decoration-none"><i class="fas fa-plus-circle"></i> Create Category</a></li>
    </ul>
</div>