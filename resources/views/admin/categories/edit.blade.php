@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @include('inc.menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header font-weight-bold">
                        {{ config('app.name') }} : Update Category
                        <a href="{{ route('home') }}" class="btn btn-default mb-2 p-0 text-muted float-right"><i class="far fa-arrow-alt-circle-left"></i> Go Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('category.update', ['id' => $category->id]) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">Category Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                                @if($errors->has('name'))
                                    <small class="text-muted">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection