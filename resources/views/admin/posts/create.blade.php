@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @include('inc.menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header font-weight-bold">
                        {{ config('app.name') }} : Post
                        <a href="{{ route('home') }}" class="btn btn-default mb-2 p-0 text-muted float-right"><i class="far fa-arrow-alt-circle-left"></i> Go Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category" id="category" class="form-control">
                                    @if ( count($categories) > 0 )
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    @else
                                        <option value="" selected disabled>No Category Available.</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                                @if($errors->has('title'))
                                    <small class="text-muted">{{ $errors->first('title') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="featured">Image</label>
                                <input type="file" name="featured" id="featured" class="form-control">
                                @if($errors->has('featured'))
                                    <small class="text-muted">{{ $errors->first('featured') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ old('content') }}</textarea>
                                @if($errors->has('content'))
                                    <small class="text-muted">{{ $errors->first('content') }}</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection