<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class PageController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(2);
        $categories = Category::all();

        return view('index')->with('posts', $posts)->with('categories', $categories);
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        $categories = Category::all();
        
        return view('show')->with('post', $post)->with('categories', $categories);
    }

    public function selectedCategory($id)
    {
        $categories = Category::all();
        $posts = Post::where('category_id', $id)->orderBy('created_at', 'desc')->paginate(2);
        
        return view('index')->with('posts', $posts)->with('categories', $categories);
    }
}
